const utils = require('./utils')

let db

export const getDB = () => {
	return db
}

export const setDB = (value) => {
	db = value
}

export const removeTask = async (name) => {
	return await db.collection('tasks').remove({ name: name })
}

export const insertTask = async (task) => {
	return await db.collection('tasks').insert(task)
}

export const findTask = async (name) => {
	return db.collection('tasks').findOne({ name: name })
}

export const getHeros = async () => {
	return db.collection('heros').find({}).toArray()
}

export const getLeaderboards = async () => {
	return db.collection('leaderboards').find({}).toArray()
}

export const setHeros = async (heros) => {
	try { await db.collection('heros').drop() } catch (e) {}
	await db.collection('heros').insertMany(heros)
}

export const setLeaderboards = async (leaderboards) => {
	try { await db.collection('leaderboards').drop() } catch (e) {}
	await db.collection('leaderboards').insertMany(leaderboards)
}
