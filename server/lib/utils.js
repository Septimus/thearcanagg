exports.requireReload = (resolvedModulePath) => {
	delete require.cache[resolvedModulePath]
	return require(resolvedModulePath)
}

exports.log = function() {
	let date = new Date()
	let year = date.getFullYear()
	let month = (date.getMonth() + 1).toString().padStart(2, '0')
	let day = date.getDate().toString().padStart(2, '0')
	let hours = date.getHours().toString().padStart(2, '0')
	let mins = date.getMinutes().toString().padStart(2, '0')
	let secs = date.getSeconds().toString().padStart(2, '0')
	let millis = date.getMilliseconds()
	let dateTimeStr = `${year}/${month}/${day} ${hours}:${mins}:${secs}:${millis} -`
	console.log.apply(console, [dateTimeStr].concat(Array.prototype.slice.call(arguments, 0)))
}

exports.sleep = (millis) => {
	return new Promise((resolve, reject) => {
		setTimeout(() => { resolve() }, millis)
	})
}
