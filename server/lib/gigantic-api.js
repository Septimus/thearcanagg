const request = require('request-promise-native')

const baseUrl = 'https://stats.gogigantic.com/en/gigantic-careers'

function escapeRequestUrl(url) {
	return encodeURI(url).replace('#', '%23')
}

exports.playerSearch = async (username, pageNum, pageSize) => {
	var query = []
	if (username) query.push(`username=${username}`)
	if (pageNum) query.push(`page_num=${pageNum}`)
	if (pageSize) query.push(`page_size=${pageSize}`)
	return await request(escapeRequestUrl(`${baseUrl}/playersearch/?${query.join('&')}`))
}

exports.usersData = async (gamertag) => {
	var query = []
	if (gamertag) query.push(`usernames[]=${gamertag}`)
	return await request(escapeRequestUrl(`${baseUrl}/usersdata/?${query.join('&')}`))
}

exports.leaderboardData = async (pageNum, pageSize, platform) => {
	var query = []
	if (pageNum) query.push(`page_num=${pageNum}`)
	if (pageSize) query.push(`page_size=${pageSize}`)
	if (platform) query.push(`platform=${platform}`)
	return await request(escapeRequestUrl(`${baseUrl}/leaderboard-data/?${query.join('&')}`))
}
