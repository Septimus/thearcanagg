const giganticApi = require('./gigantic-api')
const utils = require('./utils')

let accessor

export const setAccessor = (value) => {
	accessor = value
}

export const init = async () => {
	await exports.schedule('dailyTopStatsAggregation', dailyTopStatsAggregation)
}

export const schedule = async (name, cb, defaultTimestamp) => {
	let task = await accessor.findTask(name)

	if (!task) {
		task = { name: name, timestamp: defaultTimestamp || 0 }
		await accessor.insertTask(task)
	}

	let diff = Date.now() - task.timestamp

	if (diff > 0) diff = 0

	diff = Math.abs(diff)

	utils.log(`Running task '${name}' in ${diff / 1000} seconds`)

	setTimeout(async () => {
		await accessor.removeTask(name)
		let timestamp = await cb()
		if (timestamp) exports.schedule(name, cb, timestamp)
	}, diff)
}

function top200Data(kills, assists, deaths, damage_dealt, guardian_damage) {
	this.kills = kills,
	this.assists = assists,
	this.deaths = deaths,
	this.damage_dealt = damage_dealt,
	this.guardian_damage = guardian_damage
}

async function dailyTopStatsAggregation() {
	console.log("Aggregating Daily Top Stats")

	return Date.now() + (1000 * 60 * 60)
}
