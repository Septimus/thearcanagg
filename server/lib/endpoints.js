const giganticApi = require('./gigantic-api')
const utils = require('./utils')

let accessor

export const setAccessor = (value) => {
	accessor = value
}

exports['/v1/autocomplete/:query'] = async (req, res) => {
	let response = await giganticApi.playerSearch(req.params.query, 0, 5)
	res.end(response)
}

exports['/v1/search/:username'] = async (req, res) => {
	let response = await giganticApi.playerSearch(req.params.username, 0, 100)
	res.end(response)
}

exports['/v1/leaderboards/:platform/:page_size'] = async (req, res) => {
	let pageSize = req.params.page_size
	let platform = req.params.platform

	if (isNaN(Number(pageSize))) return res.json({ ok: false, message: 'Invalid page size' })
	if (!['all', 'arc', 'live'].includes(platform)) return res.json({ ok: false, message: 'Invalid platform' })
	if (platform == 'all') platform = ''

	try {
		let leaderboards = await accessor.getLeaderboards() || []
		let leaderboard = leaderboards.find(leaderboard => leaderboard.index == pageSize + '/' + platform)
		if (leaderboard) return res.json({ ok: true, lastUpdate: leaderboard.lastUpdated, data: leaderboard.data })
		let response = await giganticApi.leaderboardData(0, req.params.page_size, req.params.platform)
		let obj = JSON.parse(response)
		if (!obj.data) throw new Error('Invalid remote response')
		let now = Date.now()
		leaderboards.push({ index: pageSize + '/' + platform, lastUpdated: now, data: obj.data })
		await accessor.setLeaderboards(leaderboards)
		res.json({ ok: true, message: 'Last updated ' + now, data: obj.data })
	}
	catch (e) {
		console.error(e)
		return res.json({ ok: false, message: 'Failed to fetch leaderboard data' })
	}
}

exports['/v1/hero/:gamertag'] = async (req, res) => {
	let gamertag = req.params.gamertag

	if (!gamertag) return res.json({ ok: false, message: 'Invalid gamertag' })

	try {
		let heros = await accessor.getHeros() || []
		let hero = heros.find(hero => hero.index == gamertag)
		if (hero) return res.json({ ok: true, lastUpdate: hero.lastUpdated, data: hero.data })
		let response = await giganticApi.usersData(gamertag)
		let obj = JSON.parse(response)
		if (!obj.data) throw new Error('Invalid remote response')
		let now = Date.now()
		heros.push({ index: gamertag, lastUpdated: now, data: obj.data })
		await accessor.setHeros(heros)
		res.json({ ok: true, message: 'Last updated ' + now, data: obj.data })
	}
	catch (e) {
		utils.log(e)
		return res.json({ ok: false, message: 'Failed to fetch hero data' })
	}
}
