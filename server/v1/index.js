import { Router } from 'express'

import giganticApi from './giganticApi'

const router = Router()

// Add USERS Routes
router.use(giganticApi)

export default router
