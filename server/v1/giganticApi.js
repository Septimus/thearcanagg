import {Router} from 'express'
import request from 'request-promise-native'
import mongodb from 'mongodb'

const router = Router()

const baseUrl = 'https://stats.gogigantic.com/en/gigantic-careers'
var db = mongodb.MongoClient.connect('mongodb://localhost:27017/thearcana')

function escapeRequestUrl(url) {
	return encodeURI(url).replace('#', '%23')
}

/* GET users listing. */
router.get('/autocomplete/:username', async function(req, res, next) {
	var query = []
	var pageNum = 0
	var pageSize = 5
	query.push(`username=${req.params.username}`)
	if (pageNum) query.push(`page_num=${pageNum}`)
	if (pageSize) query.push(`page_size=${pageSize}`)
	var users = await request(escapeRequestUrl(`${baseUrl}/playersearch/?${query.join('&')}`))
	res.json(users)
})

router.get('/users/:username', async function(req, res, next) {
	var query = []
	query.push(`usernames[]=${req.params.username}`)
	var user = await request(escapeRequestUrl(`${baseUrl}/usersdata/?${query.join('&')}`))
	user = JSON.parse(user)
	res.json(user)
})

router.get('/playerData/:username', async function(req, res, next) {
	var query = []
	query.push(`page_num=0`)
	query.push(`page_size=500`)
	query.push(`platform=arc`)
	var user = await request(escapeRequestUrl(`${baseUrl}/leaderboard-data/?${query.join('&')}`))
	user = JSON.parse(user)
	var playerData = user.data.filter(function(obj) {
		return obj.gamertag === req.params.username
	})
	if (playerData.length === 0) {
		query = []
		query.push(`page_num=0`)
		query.push(`page_size=500`)
		query.push(`platform=live`)
		user = await request(escapeRequestUrl(`${baseUrl}/leaderboard-data/?${query.join('&')}`))
		user = JSON.parse(user)
		playerData = user.data.filter(function(obj) {
			return obj.gamertag === req.params.username
		})
	}
	if (playerData.length > 0) res.json(playerData)
	else {
		query = []
		query.push(`username=${req.params.username}`)
		query.push(`page_num=0`)
		query.push(`page_size=1`)
		user = await request(escapeRequestUrl(`${baseUrl}/playersearch/?${query.join('&')}`))
		user = JSON.parse(user)
		playerData = user.data
		playerData[0].rank = "<500"
		res.json(playerData)
	}
})

router.post('/team/', async function(req, res, next) {
	let db = await mongodb.MongoClient.connect('mongodb://localhost:27017/thearcana')
	let result = await db.collection('teams').insert(req.body)
	res.send(result)
})

export default router
