require('source-map-support/register')
module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_nuxt__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_nuxt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_nuxt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_body_parser__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_body_parser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_body_parser__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__v1__ = __webpack_require__(4);






var app = __WEBPACK_IMPORTED_MODULE_0_express___default()();
var host = process.env.HOST || '127.0.0.1';
var port = process.env.PORT || 8080;

app.set('port', port);

app.use(__WEBPACK_IMPORTED_MODULE_2_body_parser___default.a.json());
app.use(__WEBPACK_IMPORTED_MODULE_2_body_parser___default.a.urlencoded({ extended: true }));

// Import API Routes
app.use('/v1', __WEBPACK_IMPORTED_MODULE_3__v1__["a" /* default */]);

// Import and Set Nuxt.js options
var config = __webpack_require__(10);
config.dev = !("development" === 'production');

// Init Nuxt.js
var nuxt = new __WEBPACK_IMPORTED_MODULE_1_nuxt__["Nuxt"](config);

// Build only in dev mode
if (config.dev) {
  var builder = new __WEBPACK_IMPORTED_MODULE_1_nuxt__["Builder"](nuxt);
  builder.build();
}

// Give nuxt middleware to express
app.use(nuxt.render);

// Listen the server
app.listen(port, host);
console.log('Server listening on ' + host + ':' + port); // eslint-disable-line no-console

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("nuxt");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__giganticApi__ = __webpack_require__(5);




var router = Object(__WEBPACK_IMPORTED_MODULE_0_express__["Router"])();

// Add USERS Routes
router.use(__WEBPACK_IMPORTED_MODULE_1__giganticApi__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (router);

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_C_Users_Septimus_Documents_jsprojects_thearcana_node_modules_babel_runtime_regenerator__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_C_Users_Septimus_Documents_jsprojects_thearcana_node_modules_babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_C_Users_Septimus_Documents_jsprojects_thearcana_node_modules_babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_express__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_request_promise_native__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_request_promise_native___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_request_promise_native__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_mongodb__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_mongodb___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_mongodb__);


function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }





var router = Object(__WEBPACK_IMPORTED_MODULE_1_express__["Router"])();

var baseUrl = 'https://stats.gogigantic.com/en/gigantic-careers';
var db = __WEBPACK_IMPORTED_MODULE_3_mongodb___default.a.MongoClient.connect('mongodb://localhost:27017/thearcana');

function escapeRequestUrl(url) {
  return encodeURI(url).replace('#', '%23');
}

/* GET users listing. */
router.get('/autocomplete/:username', function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_C_Users_Septimus_Documents_jsprojects_thearcana_node_modules_babel_runtime_regenerator___default.a.mark(function _callee(req, res, next) {
    var query, pageNum, pageSize, users;
    return __WEBPACK_IMPORTED_MODULE_0_C_Users_Septimus_Documents_jsprojects_thearcana_node_modules_babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            query = [];
            pageNum = 0;
            pageSize = 5;

            query.push('username=' + req.params.username);
            if (pageNum) query.push('page_num=' + pageNum);
            if (pageSize) query.push('page_size=' + pageSize);
            _context.next = 8;
            return __WEBPACK_IMPORTED_MODULE_2_request_promise_native___default()(escapeRequestUrl(baseUrl + '/playersearch/?' + query.join('&')));

          case 8:
            users = _context.sent;

            res.json(users);

          case 10:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}());

router.get('/users/:username', function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_C_Users_Septimus_Documents_jsprojects_thearcana_node_modules_babel_runtime_regenerator___default.a.mark(function _callee2(req, res, next) {
    var query, user;
    return __WEBPACK_IMPORTED_MODULE_0_C_Users_Septimus_Documents_jsprojects_thearcana_node_modules_babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            query = [];

            query.push('usernames[]=' + req.params.username);
            _context2.next = 4;
            return __WEBPACK_IMPORTED_MODULE_2_request_promise_native___default()(escapeRequestUrl(baseUrl + '/usersdata/?' + query.join('&')));

          case 4:
            user = _context2.sent;

            user = JSON.parse(user);
            res.json(user);

          case 7:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, this);
  }));

  return function (_x4, _x5, _x6) {
    return _ref2.apply(this, arguments);
  };
}());

router.get('/playerData/:username', function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_C_Users_Septimus_Documents_jsprojects_thearcana_node_modules_babel_runtime_regenerator___default.a.mark(function _callee3(req, res, next) {
    var query, user, playerData;
    return __WEBPACK_IMPORTED_MODULE_0_C_Users_Septimus_Documents_jsprojects_thearcana_node_modules_babel_runtime_regenerator___default.a.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            query = [];

            query.push('page_num=0');
            query.push('page_size=500');
            query.push('platform=arc');
            _context3.next = 6;
            return __WEBPACK_IMPORTED_MODULE_2_request_promise_native___default()(escapeRequestUrl(baseUrl + '/leaderboard-data/?' + query.join('&')));

          case 6:
            user = _context3.sent;

            user = JSON.parse(user);
            playerData = user.data.filter(function (obj) {
              return obj.gamertag === req.params.username;
            });

            if (!(playerData.length === 0)) {
              _context3.next = 19;
              break;
            }

            query = [];
            query.push('page_num=0');
            query.push('page_size=500');
            query.push('platform=live');
            _context3.next = 16;
            return __WEBPACK_IMPORTED_MODULE_2_request_promise_native___default()(escapeRequestUrl(baseUrl + '/leaderboard-data/?' + query.join('&')));

          case 16:
            user = _context3.sent;

            user = JSON.parse(user);
            playerData = user.data.filter(function (obj) {
              return obj.gamertag === req.params.username;
            });

          case 19:
            if (!(playerData.length > 0)) {
              _context3.next = 23;
              break;
            }

            res.json(playerData);
            _context3.next = 34;
            break;

          case 23:
            query = [];
            query.push('username=' + req.params.username);
            query.push('page_num=0');
            query.push('page_size=1');
            _context3.next = 29;
            return __WEBPACK_IMPORTED_MODULE_2_request_promise_native___default()(escapeRequestUrl(baseUrl + '/playersearch/?' + query.join('&')));

          case 29:
            user = _context3.sent;

            user = JSON.parse(user);
            playerData = user.data;
            playerData[0].rank = "<500";
            res.json(playerData);

          case 34:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, this);
  }));

  return function (_x7, _x8, _x9) {
    return _ref3.apply(this, arguments);
  };
}());

router.post('/team/', function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/__WEBPACK_IMPORTED_MODULE_0_C_Users_Septimus_Documents_jsprojects_thearcana_node_modules_babel_runtime_regenerator___default.a.mark(function _callee4(req, res, next) {
    var db, result;
    return __WEBPACK_IMPORTED_MODULE_0_C_Users_Septimus_Documents_jsprojects_thearcana_node_modules_babel_runtime_regenerator___default.a.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return __WEBPACK_IMPORTED_MODULE_3_mongodb___default.a.MongoClient.connect('mongodb://localhost:27017/thearcana');

          case 2:
            db = _context4.sent;
            _context4.next = 5;
            return db.collection('teams').insert(req.body);

          case 5:
            result = _context4.sent;

            res.send(result);

          case 7:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, this);
  }));

  return function (_x10, _x11, _x12) {
    return _ref4.apply(this, arguments);
  };
}());

/* harmony default export */ __webpack_exports__["a"] = (router);

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(7);


/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("regenerator-runtime");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("request-promise-native");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("mongodb");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'thearcana.gg',
    meta: [{ charset: 'utf-8' }, { name: 'viewport', content: 'width=device-width, initial-scale=1' }, { hid: 'description', name: 'description', content: 'thearcana.gg GoGigantic leaderboards, data, and competitive.' }],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }]
  },
  /*
  ** Global CSS
  */
  css: ['~/assets/css/main.scss'],
  /*
  ** Add axios globally
  */
  build: {
    vendor: ['axios'],
    /*
    ** Run ESLINT on save
    */
    extend: function extend(config, ctx) {
      if (ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    }
  }
};

/***/ })
/******/ ]);
//# sourceMappingURL=main.map